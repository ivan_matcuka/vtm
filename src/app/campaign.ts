import {TargetField} from './target';

interface CampaignFrequency {
  active: boolean;
  number: number;
  type: string;
  frequency: string;
}

export declare type CampaignSettings = TargetField[];

export interface CreateCampaignRequestBody {
  // account: string;
  folderID: number;
  campaign: string;
  budget: number;
  budgetType: string;
  campaignType: string;
  networks: string;
  // languages: string;
  bidStrategyType: string;
  // bidStrategyName: string;
  startDate: string;
  endDate: string;
  // adSchedule: string;
  // adRotation: string;
  // inventoryType: string;
  // deliveryMethod: string;
  // targetingMethod: string;
  // exclusionMethod: string;
  // dsaWebsite: string;
  // dsaLanguage: string;
  // dsaTargetingSource: string;
  // dsaPageFeeds: string;
  // flexibleReach: string;
  // location: string;
  // maxCPC: number;
  maxCPM: number;
  targetCPA: string;
  maxCPV: number;
  targetCPM: number;
}

export class Campaign {
  campaign: string;
  budget: number;
  budgetType: string;
  campaignType: string;
  startDate: string;
  endDate: string;
  frequency: CampaignFrequency;
  bidStrategyType: string;
  maxCPM: number;
  maxCPV: number;
  targetCPA: string;
  targetCPM: number;
  networks = {
    search: {
      title: 'YouTube Search',
      value: false,
    },
    videos: {
      title: 'YouTube Videos',
      value: false,
    },
    partners: {
      title: 'Video Partners',
      value: false,
    },
  };

  public mapNetworks() {
    return Object.values(this.networks)
      .filter((network): any => {
        return network.value;
      })
      .map((network): any => {
        return network.title;
      })
      .join(';');
  }
}

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Target, TargetField, TargetOptions} from '../target';

interface Searcher {
  [key: string]: string;
}

@Component({
  selector: 'app-target-row',
  templateUrl: './target-row.component.html',
  styleUrls: ['./target-row.component.styl']
})
export class TargetRowComponent {
  @Input() active: boolean;
  @Input() fields: TargetField[];
  @Input() target: Target;
  @Input() generator = true;
  @Input() checked: boolean;
  @Input() key: number;
  @Input() ruleMask: number[];
  @Output() activate = new EventEmitter<number>();
  @Output() deactivate = new EventEmitter<number>();
  @Output() changed = new EventEmitter<void>();

  public list: string;
  public searcher: Searcher = {};

  constructor() {
  }

  customTrackBy(index: number, _: any): any {
    return index;
  }

  setActiveList(fieldName) {
    if (this.list === fieldName) {
      delete this.list;
    } else {
      this.list = fieldName;
    }
  }

  isMore(field) {
    const length = this.target[field].length;
    const last = this.target[field][length - 1];

    return length === 0 || last;
  }

  isChecked(target, field, option) {
    return target[field].includes(option);
  }

  handleCheck() {
    if (this.checked) {
      this.deactivate.emit(this.key);
    } else {
      this.activate.emit(this.key);
    }
  }

  public setOption(event: any, field) {
    const checked = event.target.checked;
    const value = event.target.value;

    if (checked) {
      this.target[field] = [...new Set([...this.target[field], value])];
    } else {
      this.target[field] = this.target[field].filter((el: number) => el !== value);
    }

    this.changed.emit();
  }

  public isList(field: TargetField): boolean {
    return Boolean(field.options);
  }

  public addValue(field) {
    this.target[field].length += 1;

    this.changed.emit();
  }

  public toString(field: string): string {
    const activeOptionIds = this.target[field] as string[];

    return activeOptionIds.join(', ');
  }

  /**
   * Handles paste event.
   * Splits the string by the "\n" symbol
   * and pastes each element into a separate row.
   *
   * @param event - Event object
   * @param field - Field to paste to
   * @param i - An index (TODO: why?)
   */
  public handlePaste(event: any, field: string, i: number) {
    event.preventDefault();

    const words = event.clipboardData
      .getData('text')
      .split(/[\n]/gm);

    this.target[field].pop();
    this.target[field] = [...this.target[field], ...words];

    this.changed.emit();
  }

  /**
   * Deletes the row from a non-discrete list.
   *
   * @param field - Field name
   * @param i - Row index in the array
   */
  public deleteRow(field: string, i: number) {
    this.target[field] = this.target[field].filter((_, index: number) => {
      return index !== i;
    });

    this.changed.emit();
  }


  /**
   * Blurs inputs.
   *
   * @param e - Keyboard event object
   * @param el - HTL input element
   */
  public blurInput(e: KeyboardEvent, el: HTMLInputElement) {
    if (e.key === 'Enter') {
      el.blur();
    }
  }

  /**
   * Handles clicks outside the component
   * to fold open dropdown lists.
   *
   * @param e - Event object
   */
  public handleClickOutside(e: any) {
    if (!e.target.classList.contains('target-row__more-button')) {
      this.list = undefined;
    }
  }

  /**
   * Filters options by the "searcher" object.
   *
   * @param fieldKey - Field name to filter
   * @param options - Options object
   */
  public filterOptions(fieldKey: string, options: TargetOptions): TargetOptions {
    const object: TargetOptions = {};

    Object.keys(options).forEach((key: string) => {
      if (!this.searcher[fieldKey]) {
        object[key] = options[key];
        return;
      }

      const option = options[key].toLowerCase();
      const query = this.searcher[fieldKey].toLowerCase();

      if (option.includes(query)) {
        object[key] = options[key];
      }
    });

    return object;
  }
}

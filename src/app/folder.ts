export type FolderType = 'audience' | 'matching' | 'video';

export interface Folder {
  id?: number;
  name: string;
  type: FolderType;
  createdAt?: string;
}

export interface AudienceFolder extends Folder {
  BLSSplit: boolean;
  frequency: string;
  rulesMask: string;
  campaignID?: number;
  techAdGroupID?: number;
}

export interface MatchingFolder extends Folder {
  videoFolderID: number;
  videoFolderName?: string;
  audienceFolderID: number;
  audienceFolderName?: string;
}

export interface VideoFolder extends Folder {
}

export interface Rule {
  name: string;
  static: number;
}

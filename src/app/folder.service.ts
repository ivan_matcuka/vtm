import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Folder, MatchingFolder} from './folder';
import {CreateCampaignRequestBody} from './campaign';

@Injectable({
  providedIn: 'root'
})
export class FolderService {
  private baseURL = 'http://vtm.polymorphy.ru:8080';

  constructor(private http: HttpClient) {
  }

  /**
   * Fetches all folders from the server.
   *
   * @return List of all folders.
   */
  public all(type: string): Observable<any> {
    // const options = {
    //   headers: new HttpHeaders({
    //     'X-Auth-Token': 'secret',
    //   })
    // };

    return this.http.get(`${this.baseURL}/folders/?type=${type}`);
  }

  public matching(): Observable<any> {
    return this.http.get(`${this.baseURL}/folders/match`);
  }

  public createMatching(matchingFolder: MatchingFolder) {
    return this.http.post(`${this.baseURL}/folders/match`, matchingFolder);
  }

  public deleteMatching(matchingFolderId: number) {
    return this.http.delete(`${this.baseURL}/folders/match/${matchingFolderId}`);
  }

  public folder(id: number): Observable<any> {
    return this.http.get(`${this.baseURL}/folders/${id}`);
  }

  public create(folder: Folder): Observable<any> {
    return this.http.post(`${this.baseURL}/folders`, folder);
  }

  public delete(id: number): Observable<any> {
    return this.http.delete(`${this.baseURL}/folders/${id}`);
  }

  public rules(): Observable<any> {
    return this.http.get(`${this.baseURL}/settings/adgroup:settings`);
  }

  public options(): Observable<any> {
    return this.http.get(`${this.baseURL}/settings/object`);
  }

  public videoFolder(folderId): Observable<any> {
    return this.http.get(`${this.baseURL}/video/folders/${folderId}`);
  }

  public campaignParams(): Observable<any> {
    return this.http.get(`${this.baseURL}/settings/tech`);
  }

  public createCampaign(requestBody: CreateCampaignRequestBody): Observable<any> {
    return this.http.post(`${this.baseURL}/campaign`, requestBody);
  }
}


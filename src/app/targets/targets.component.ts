// Framework
import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';

// Interfaces
import {ArrayResourceResponse, ResourceResponse} from '../interfaces/foldersResponse';
import {SaveTargetsRequest, Target, TargetField} from '../target';
import {AudienceFolder} from '../folder';

// Services
import {FolderService} from '../folder.service';
import {TargetService} from '../target.service';

@Component({
  selector: 'app-targets',
  templateUrl: './targets.component.html',
  styleUrls: ['./targets.component.styl']
})
export class TargetsComponent implements OnInit {
  /**
   * Base service URL.
   */
  private baseURL = 'http://vtm.polymorphy.ru:8080';

  public generator: Target = new Target();
  public rules: string[] = [];
  public folder: AudienceFolder;
  public rulesArray: number[] = [];
  public submitType = 'generate';
  public targets: Target[] = [];
  public deletedTargets: number[] = [];
  public generateNumber = 1;
  public fields: TargetField[] = [];
  public activeRows: number[] = [];
  public oldIds: number[] = [];
  public saved = true;
  public fold = true;

  constructor(
    private folderService: FolderService,
    private route: ActivatedRoute,
    private targetService: TargetService,
  ) {
  }

  /**
   * Parses the rule mask
   *
   * @param rulesMask - Binary mask as a string
   * @return An array of numbers
   */
  private static parseMask(rulesMask: string): number[] {
    return rulesMask.split('').map((rule: string) => +rule);
  }

  /** @inheritDoc */
  public ngOnInit(): void {
    this.fetchFolderData();
    this.fetchOptions();
  }

  /**
   * Fetches information about the folder.
   */
  private fetchFolderData(): void {
    this.folderService.folder(this.route.snapshot.params.id)
      .subscribe((response: ResourceResponse<AudienceFolder>) => {
        this.rulesArray = TargetsComponent.parseMask(response.data.rulesMask);
        console.log(TargetsComponent.parseMask(response.data.rulesMask));
        this.folder = response.data;

        this.fetchTargets();
      });
  }

  /**
   * Fetches all targets that
   * belong to the folder.
   */
  private fetchTargets() {
    this.targetService.byFolder(this.folder.id)
      .subscribe((response: ResourceResponse<Target[]>) => {
        this.targets = response.data;
        this.oldIds = response.data.map((target: Target) => {
          return target.id;
        });
      });
  }

  /**
   * Fetches target fields.
   */
  private fetchOptions(): void {
    this.folderService.options()
      .subscribe((fields: TargetField[]) => {
        this.fields = fields;
      });
  }

  /**
   * Submits the generator.
   */
  public submit(): void {
    this.saved = false;

    switch (this.submitType) {
      case 'generate':
        this.generate();
        break;
      case 'checked':
        this.applyToChecked();
        break;
      case 'all':
        this.applyToAll();
        break;
      case 'delete':
        this.delete();
        break;
      default:
        console.error('Generator error!');
    }
  }

  /**
   * Generates the specified number of targets.
   */
  public generate(): void {
    for (let i = 0; i < this.generateNumber; i++) {
      this.targets = [...this.targets, JSON.parse(JSON.stringify(this.generator))];
    }
  }

  /**
   * Applies the settings to the active
   * targets.
   */
  public applyToChecked(): void {
    this.activeRows.forEach((rowId: number) => {
      const newTarget = JSON.parse(JSON.stringify(this.generator)) as Target;

      if (this.targets[rowId].id) {
        newTarget.id = this.targets[rowId].id;
      }

      this.targets[rowId] = newTarget;
    });
  }

  /**
   * Applies the settings to all rows.
   */
  public applyToAll(): void {
    for (let i = 0; i < this.targets.length; i++) {
      const newTarget = JSON.parse(JSON.stringify(this.generator)) as Target;

      if (this.targets[i].id) {
        newTarget.id = this.targets[i].id;
      }

      this.targets[i] = newTarget;
    }
  }

  /**
   * Deletes the active (checked) targets.
   */
  public delete(): void {
    this.activeRows.forEach((rowId: number) => {
      if (this.targets[rowId].id) {
        this.deletedTargets.push(this.targets[rowId].id);
      }
    });

    this.targets = this.targets.filter((_: Target, i: number) => {
      return !this.activeRows.includes(i);
    });

    this.activeRows = [];
  }

  /**
   * Stores or updates the targets.
   */
  public save(): void {
    this.deletedTargets.forEach((targetId: number) => {
      this.targetService.delete(targetId)
        .subscribe(() => {
          console.log('Target deleted!');
        });
    });

    const targets: SaveTargetsRequest[] = this.targets.map((target: Target) => {
      return {
        campaignID: this.folder.campaignID,
        folderID: this.folder.id,
        techAdGroupID: this.folder.techAdGroupID,
        ...target
      };
    });

    this.targetService.save(targets)
      .subscribe((response: ArrayResourceResponse<Target>) => {
        this.targets = response.data;
      });

    this.saved = true;
  }

  /**
   * Downloads the CSV file.
   */
  public download(): void {
    window.open(
      `${this.baseURL}/csv/?folderID=${this.folder.id}&campaignID=${this.folder.campaignID}`,
      '_blank'
    );
  }

  /**
   * Checks if the row is active (checked).
   *
   * @param key - Key of the row (identifier)
   */
  public isActive(key: number): boolean {
    return this.activeRows.includes(key);
  }

  /**
   * Activates the row (checkbox).
   *
   * @param rowId - ID of the row to activate
   */
  public activateRow(rowId: number): void {
    this.activeRows.push(rowId);
  }

  /**
   * Deactivates the row (checkbox).
   *
   * @param rowId - ID of the row to
   * deactivate
   */
  public deactivateRow(rowId: number): void {
    this.activeRows = this.activeRows.filter((el: number) => {
      return el !== rowId;
    });
  }

  public export(): void {
    window.open(
      `${this.baseURL}/export/targets/?folderID=${this.folder.id}&campaignID=${this.folder.campaignID}`,
      '_blank'
    );
  }

  public handleImport(e): void {
    const file: File = e.target.files[0];
    const formData = new FormData();

    if (!file) {
      return;
    }

    formData.append('uploadFile', file);

    this.targetService.import(formData)
      .subscribe(() => {
        location.reload();
      });
  }

  public handleFold(e: MouseEvent) {
    e.preventDefault();

    this.fold = !this.fold;
  }
}

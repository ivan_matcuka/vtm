import {Component, OnInit} from '@angular/core';
import {AudienceFolder, Folder, VideoFolder} from '../folder';
import {FolderService} from '../folder.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Title} from '@angular/platform-browser';
import {VideoFolderFormComponent} from '../forms/video-folder-form/video-folder-form.component';

// Libs
import moment from 'moment/src/moment';
import {ArrayResourceResponse, ResourceResponse} from '../interfaces/foldersResponse';

@Component({
  selector: 'app-video-folders',
  templateUrl: './video-folders.component.html',
  styleUrls: ['./video-folders.component.styl']
})
export class VideoFoldersComponent implements OnInit {
  public title = 'vtm';
  folders: VideoFolder[];

  public constructor(
    private folderService: FolderService,
    private modalService: NgbModal,
    private titleService: Title,
  ) {
  }

  public openAddForm() {
    const modelRef = this.modalService.open(VideoFolderFormComponent);

    modelRef.componentInstance.created.subscribe((videoFolder: VideoFolder) => {
      this.folders = [videoFolder, ...this.folders];
      modelRef.close();
    });
  }

  public ngOnInit() {
    this.folderService
      .all('video')
      .subscribe((response: ArrayResourceResponse<VideoFolder>) => {
        this.folders = response.data;
      });

    this.titleService.setTitle('Video Folders');
  }


  /**
   * Deletes the folder and resets the
   * "folders" state.
   *
   * @param id - Folder numeric ID
   */
  public deleteFolder(id: number): void {
    this.folderService.delete(id)
      .subscribe(() => {
        this.folders = this.folders.filter((folder: VideoFolder) => {
          return folder.id !== id;
        });
      });
  }

  /**
   * Returns a formatted date string.
   *
   * @param date - Raw date string
   */
  public formatDate(date: string): string {
    return moment(date).format('MMM Do YY');
  }
}

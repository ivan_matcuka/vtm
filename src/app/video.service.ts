import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

export interface VideoRequestBody {
  // TODO: RENAME
  uploadFile: File[];
  folderID: number;
  creativeID;
}

@Injectable({
  providedIn: 'root'
})
export class VideoService {
  private baseURL = 'http://vtm.polymorphy.ru:8080';

  constructor(private http: HttpClient) {
  }

  public upload(videos: FormData): Observable<any> {
    return this.http.post(`${this.baseURL}/video`, videos);
  }
}

import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Folder} from './folder';

@Injectable({
  providedIn: 'root'
})
export class VideoFolderService {

  constructor(private http: HttpClient) {
  }

  public all(): Observable<any> {
    return of([
      {
        id: 1,
        name: 'Video Folder #1',
      },
      {
        id: 2,
        name: 'Video Folder #2',
      },
    ]);
  }

  // public create(name: string): Observable<any> {
  //   // return this.http.post('/folders', folder);
  //
  //   const newFolder: Folder = {
  //     id: 4,
  //     name,
  //     type: 'video',
  //     createdAt: '2019-08-23',
  //     rulesMask: '0000000000000',
  //   };
  //
  //   return of(newFolder);
  // }
}

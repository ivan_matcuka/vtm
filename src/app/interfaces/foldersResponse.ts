export interface ErrorResponse {
  errorMess?: string;
  errorCode?: string;
}

export interface ResourceResponse<T> extends ErrorResponse {
  data: T;
}

export interface ArrayResourceResponse<T> extends ErrorResponse {
  data: T[];
}

import {Component, OnInit} from '@angular/core';
import {MatchingFolder} from '../folder';
import {FolderService} from '../folder.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {MatchingFolderFormComponent} from '../forms/matching-folder-form/matching-folder-form.component';
import {UploadMatchingFormComponent} from '../forms/upload-matching-form/upload-matching-form.component';

@Component({
  selector: 'app-matching',
  templateUrl: './matching.component.html',
  styleUrls: ['./matching.component.styl']
})
export class MatchingComponent implements OnInit {
  public folders: MatchingFolder[] = [];

  constructor(
    private folderService: FolderService,
    private modalService: NgbModal,
  ) {
  }

  ngOnInit() {
    this.folderService.matching()
      .subscribe((response: MatchingFolder[]) => {
        this.folders = response;
      });
  }

  public deleteFolder(id: number): void {
    this.folderService.deleteMatching(id)
      .subscribe(() => {
        this.folders = this.folders.filter((folder: MatchingFolder) => {
          return folder.id !== id;
        });
      });
  }

  public openNewForm() {
    const modelRef = this.modalService.open(MatchingFolderFormComponent);

    modelRef.componentInstance.created.subscribe((matchingFolder: MatchingFolder) => {
      this.folders = [matchingFolder, ...this.folders];
      modelRef.close();
    });
  }

  public openUploadForm() {
    const modelRef = this.modalService.open(UploadMatchingFormComponent);

    modelRef.componentInstance.created.subscribe(() => {
      modelRef.close();
    });
  }

}

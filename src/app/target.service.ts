import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {SaveTargetsRequest, Target} from './target';

@Injectable({
  providedIn: 'root'
})
export class TargetService {
  /**
   * Base service URL.
   */
  private baseURL = 'http://vtm.polymorphy.ru:8080';

  constructor(private http: HttpClient) {
  }

  /**
   * Fetches all adgroups (aka targets).
   */
  public all(): Observable<any> {
    return this.http.get(`${this.baseURL}/adgroup`);
  }

  /**
   * Fetches all targets associated with the
   * folder.
   *
   * @param folderId - ID of the folder associated
   * with the targets
   */
  public byFolder(folderId: number) {
    return this.http.get(`${this.baseURL}/adgroup/?folderID=${folderId}`);
  }

  /**
   * Stores or updates the targets.
   *
   * @param saveTargetsRequest - Save request
   * containing targets to update or store
   */
  public save(saveTargetsRequest: SaveTargetsRequest[]): Observable<any> {
    return this.http.post(`${this.baseURL}/adgroup`, saveTargetsRequest);
  }

  /**
   * Deletes the target.
   *
   * @param targetID - ID of the target to remove
   */
  public delete(targetID: number): Observable<any> {
    return this.http.delete(`${this.baseURL}/adgroup/${targetID}`);
  }

  /**
   * Imports settings.
   *
   * @param formData - Form data
   */
  public import(formData: FormData): Observable<any> {
    return this.http.post(`${this.baseURL}/import/targets`, formData);
  }
}

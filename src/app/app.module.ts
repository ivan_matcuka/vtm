import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule} from '@angular/common/http';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {ClickOutsideModule} from 'ng-click-outside';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AudienceFolderFormComponent} from './forms/audience-folder-form/audience-folder-form.component';
import {FormsModule} from '@angular/forms';
import {LayoutComponent} from './layout/layout.component';
import {HeaderComponent} from './header/header.component';
import {MainComponent} from './main/main.component';
import {NavigatorComponent} from './navigator/navigator.component';
import {AudiencesComponent} from './audiences/audiences.component';
import {RouterModule, Routes} from '@angular/router';
import {VideoFoldersComponent} from './video-folders/video-folders.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {VideoFolderFormComponent} from './forms/video-folder-form/video-folder-form.component';
import {TargetsComponent} from './targets/targets.component';
import {TargetRowComponent} from './target-row/target-row.component';
import {MatchingComponent} from './matching/matching.component';
import {MatchingFolderFormComponent} from './forms/matching-folder-form/matching-folder-form.component';
import {UploadMatchingFormComponent} from './forms/upload-matching-form/upload-matching-form.component';
import {VideoFolderComponent} from './video-folder/video-folder.component';
import {VideoFormComponent} from './forms/video-form/video-form.component';

const appRoutes: Routes = [
  {path: 'dashboard', component: DashboardComponent, data: {header: 'Dashboard'}},
  {path: 'videos', component: VideoFoldersComponent, data: {header: 'Video Folders'}},
  {path: 'audiences', component: AudiencesComponent, data: {header: 'Audience Folders'}},
  {path: 'folders/:id', component: TargetsComponent, data: {header: 'Folder'}},
  {path: 'videos/:id', component: VideoFolderComponent, data: {header: 'Video Folder'}},
  {path: 'matching', component: MatchingComponent, data: {header: 'Matching'}},
  {path: '', redirectTo: '/dashboard', pathMatch: 'full'},
];

@NgModule({
  declarations: [
    AppComponent,
    AudienceFolderFormComponent,
    LayoutComponent,
    HeaderComponent,
    MainComponent,
    NavigatorComponent,
    AudiencesComponent,
    VideoFoldersComponent,
    DashboardComponent,
    VideoFolderFormComponent,
    TargetsComponent,
    MatchingComponent,
    MatchingFolderFormComponent,
    UploadMatchingFormComponent,
    VideoFolderComponent,
    VideoFormComponent,
    TargetRowComponent,
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {enableTracing: false}
    ),
    BrowserModule,
    ClickOutsideModule,
    AppRoutingModule,
    NgbModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [AudienceFolderFormComponent, MatchingFolderFormComponent, VideoFolderFormComponent, VideoFormComponent],
})
export class AppModule {
}

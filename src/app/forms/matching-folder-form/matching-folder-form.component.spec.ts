import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatchingFolderFormComponent } from './matching-folder-form.component';

describe('MatchingFormComponentComponent', () => {
  let component: MatchingFolderFormComponent;
  let fixture: ComponentFixture<MatchingFolderFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatchingFolderFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatchingFolderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

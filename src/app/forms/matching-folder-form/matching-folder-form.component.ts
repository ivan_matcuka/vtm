import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {AudienceFolder, MatchingFolder, Rule, VideoFolder} from '../../folder';
import {FolderService} from '../../folder.service';
import {ArrayResourceResponse} from '../../interfaces/foldersResponse';

interface NewFormSettings {
  name: string;
  videoFolderID: number;
  audienceFolderID: number;
}

@Component({
  selector: 'app-matching-form-component',
  templateUrl: './matching-folder-form.component.html',
  styleUrls: ['./matching-folder-form.component.styl']
})
export class MatchingFolderFormComponent implements OnInit {
  @Output() created = new EventEmitter<MatchingFolder[]>();

  public audienceFolders: AudienceFolder[];
  public videoFolders: VideoFolder[];

  public newFormSettings: NewFormSettings = {
    name: '',
    videoFolderID: null,
    audienceFolderID: null,
  };

  constructor(private folderService: FolderService) {
  }

  onSubmit() {
    const newFolder: MatchingFolder = {
      name: this.newFormSettings.name,
      audienceFolderID: +this.newFormSettings.audienceFolderID,
      type: 'matching',
      videoFolderID: +this.newFormSettings.videoFolderID
    };

    this.folderService.createMatching(newFolder)
      .subscribe((response: ArrayResourceResponse<MatchingFolder>) => {
        this.created.emit(response.data);
      });

  }

  ngOnInit() {
    this.folderService.all('audience')
      .subscribe((response: ArrayResourceResponse<AudienceFolder>) => {
        this.audienceFolders = response.data;
      });

    this.folderService.all('video')
      .subscribe((response: ArrayResourceResponse<VideoFolder>) => {
        this.videoFolders = response.data;
      });
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoFolderFormComponent } from './video-form.component';

describe('VideoFolderFormComponent', () => {
  let component: VideoFolderFormComponent;
  let fixture: ComponentFixture<VideoFolderFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoFolderFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoFolderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

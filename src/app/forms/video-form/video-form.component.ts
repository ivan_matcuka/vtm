import {Component, Directive, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FolderService} from '../../folder.service';
import {AudienceFolder, VideoFolder} from '../../folder';
import {ArrayResourceResponse, ResourceResponse} from '../../interfaces/foldersResponse';
import {VideoRequestBody, VideoService} from '../../video.service';
import {ActivatedRoute} from '@angular/router';
import {Video} from '../../video';

interface VideoSettings {
  title: string;
  description: string;
  videos: File[];
}

@Component({
  selector: 'app-video-folder-form',
  templateUrl: './video-form.component.html',
  styleUrls: ['./video-form.component.styl']
})
export class VideoFormComponent implements OnInit {
  @Input() folderId: number;

  /**
   * Event emitter.
   * Fired when a folder is created.
   */
  @Output() created = new EventEmitter<Video[]>();

  public videoSettings: VideoSettings = {
    title: '',
    description: '',
    videos: [],
  };

  public ngOnInit(): void {
    console.log(this.route.snapshot.params);
  }

  constructor(
    private videoService: VideoService,
    private route: ActivatedRoute,
  ) {
  }

  public onSubmit() {
    const videoFormData: FormData = new FormData();

    videoFormData.append('folderID', String(this.folderId));
    videoFormData.append('title', this.videoSettings.title);
    videoFormData.append('description', this.videoSettings.description);

    this.videoSettings.videos.forEach((video: File) => {
      videoFormData.append('uploadFile[]', video);
    });

    this.videoService.upload(videoFormData)
      .subscribe((response: Video[]) => {
        this.created.emit(response);
      });
  }

  /**
   * Returns a button string
   * depending on the form state.
   *
   * @return Number of files selected if any
   * or a placeholder otherwise
   */
  public showButtonText(): string {
    const videos = this.videoSettings.videos;

    return videos.length ? `${videos.length} Files Selected` : 'Video Files';
  }

  public handleCustomFiles(event: any) {
    this.videoSettings.videos = [...event.target.files];
  }

}

import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {AudienceFolderFormComponent} from './audience-folder-form.component';

describe('FolderFormComponent', () => {
  let component: AudienceFolderFormComponent;
  let fixture: ComponentFixture<AudienceFolderFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AudienceFolderFormComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AudienceFolderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

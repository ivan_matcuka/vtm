// Framework
import {Component, OnInit, Output, EventEmitter} from '@angular/core';

// Services
import {VideoFolderService} from '../../video-folder.service';
import {FolderService} from '../../folder.service';

// Interfaces
import {Campaign, CampaignSettings, CreateCampaignRequestBody} from '../../campaign';
import {TargetField} from '../../target';
import {AudienceFolder, Folder, Rule} from '../../folder';
import {ArrayResourceResponse, ResourceResponse} from '../../interfaces/foldersResponse';

/**
 * Describes audience folder settings.
 */
interface FolderSettings {
  /**
   * Whether "BLS" mode is on.
   */
  bls: boolean;

  /**
   * Custom frequency type.
   */
  frequency: {

    /**
     * Whether the "frequency" mode is active.
     */
    active: boolean;

    /**
     * Number of events.
     */
    number?: number;

    /**
     * Event type (impressions or views).
     */
    type?: number;

    /**
     * Frequency value (per day/week/month).
     */
    frequency?: number;
  };

  /**
   * Folder name.
   */
  name?: string;

  /**
   * Folder discrete options.
   */
  options: {

    /**
     * Frequency event types.
     */
    types: string[];

    /**
     * Frequency values.
     */
    frequencies: string[];
  };
}

@Component({
  selector: 'app-folder-form',
  templateUrl: './audience-folder-form.component.html',
  styleUrls: ['./audience-folder-form.component.styl']
})
export class AudienceFolderFormComponent implements OnInit {
  /**
   * Event emitter.
   * Fired when a folder is created.
   */
  @Output() created = new EventEmitter<AudienceFolder>();

  /**
   * Folder initial settings.
   */
  public folderSettings: FolderSettings = {
    bls: false,
    frequency: {
      active: false,
    },
    options: {
      types: ['impressions', 'views'],
      frequencies: ['per day', 'per week', 'per month'],
    }
  };

  /**
   * Whether video-based or not.
   */
  public type: boolean;

  /**
   * Form step (max: 3).
   */
  public step = 1;

  /**
   * Dynamic/static field rules.
   */
  public rules: Rule[] = [];

  /**
   * Current video folder.
   */
  public videoFolder: number = null;

  /**
   * Video folders.
   * Used to create video-based folders.
   */
  public videoFolders: Folder[];

  /**
   * Empty campaign instance.
   */
  public campaign: Campaign = new Campaign();

  /**
   * Campaign settings.
   */
  public campaignSettings: CampaignSettings;

  constructor(
    private folderService: FolderService,
    private videoFolderService: VideoFolderService,
  ) {
  }

  /**
   * Submits the form.
   * Creates a folder and one technical campaign.
   */
  public onSubmit() {
    // Object destructuring
    const [_, fNumber, type, frequency] = Object.values(this.folderSettings.frequency);

    // Setting the frequency
    let frequencyValue = null;
    if (this.folderSettings.frequency.active) {
      frequencyValue = [fNumber, type, frequency].join('_');
    }

    // Creating a "AudienceFolder" instance
    const newFolder: AudienceFolder = {
      BLSSplit: this.folderSettings.bls,
      frequency: frequencyValue,
      name: this.folderSettings.name,
      type: 'audience',
      rulesMask: this.rules.map((rule: Rule) => {
        return +rule.static;
      }).join(''),
    };

    // Saving the folder
    this.folderService.create(newFolder)
      .toPromise()
      .then((response: ResourceResponse<AudienceFolder>) => {
        // Creating a technical campaign
        const campaign: CreateCampaignRequestBody = {
          campaign: this.campaign.campaign,
          budget: +this.campaign.budget,
          budgetType: this.campaign.budgetType,
          campaignType: this.campaign.campaignType,
          networks: this.campaign.mapNetworks(),
          bidStrategyType: this.campaign.bidStrategyType,
          folderID: response.data.id,
          startDate: this.campaign.startDate,
          endDate: this.campaign.endDate,
          maxCPM: +this.campaign.maxCPM,
          targetCPA: this.campaign.targetCPA,
          maxCPV: +this.campaign.maxCPV,
          targetCPM: +this.campaign.targetCPM,
        };

        // Saving the campaign
        this.folderService.createCampaign(campaign)
          .subscribe(() => {
            this.created.emit(response.data);
          });
      });
  }

  /** @inheritDoc */
  public ngOnInit() {
    this.videoFolderService.all()
      .subscribe((folders: Folder[]) => {
        this.videoFolders = folders;
      });

    this.folderService.options()
      .subscribe((response) => {
        this.rules = response.map((rule: TargetField) => {
          return {
            name: rule.name,
            static: 0,
          };
        });
      });

    this.folderService.campaignParams()
      .subscribe((response: CampaignSettings) => {
        this.campaignSettings = response;
      });
  }

  /**
   * Decrements the step.
   */
  public previousStep() {
    this.step -= 1;
  }

  /**
   * Increments the step.
   */
  public nextStep() {
    this.step += 1;
  }

  /**
   * Returns the option by its name.
   *
   * @param name - Option key name
   */
  public getOptionsByName(name: string): TargetField {
    return this.campaignSettings.find((field: TargetField) => {
      return field.key === name;
    });
  }

  public setRuleValue(rule: Rule, value: number) {
    rule.static = value;
  }
}

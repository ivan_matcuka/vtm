import {Component, Directive, EventEmitter, OnInit, Output} from '@angular/core';
import {FolderService} from '../../folder.service';
import {AudienceFolder, VideoFolder} from '../../folder';
import {ResourceResponse} from '../../interfaces/foldersResponse';
import {VideoRequestBody, VideoService} from '../../video.service';

interface FolderSettings {
  title: string;
  description: string;
  videos: File[];
}

@Component({
  selector: 'app-video-folder-form',
  templateUrl: './video-folder-form.component.html',
  styleUrls: ['./video-folder-form.component.styl']
})
export class VideoFolderFormComponent {
  /**
   * Event emitter.
   * Fired when a folder is created.
   */
  @Output() created = new EventEmitter<VideoFolder>();

  public folderSettings: FolderSettings = {
    title: '',
    description: '',
    videos: [],
  };

  constructor(
    private folderService: FolderService,
    private videoService: VideoService,
  ) {
  }

  public onSubmit() {
    const newFolder: VideoFolder = {
      name: this.folderSettings.title,
      type: 'video',
    };

    this.folderService.create(newFolder)
      .subscribe((response: ResourceResponse<VideoFolder>) => {
        const videoFormData: FormData = new FormData();
        videoFormData.append('folderID', String(response.data.id));
        videoFormData.append('title', this.folderSettings.title);
        videoFormData.append('description', this.folderSettings.description);

        this.folderSettings.videos.forEach((video: File) => {
          videoFormData.append('uploadFile[]', video);
        });

        this.videoService.upload(videoFormData)
          .subscribe(() => {
            this.created.emit(response.data);
          });
      });
  }

  /**
   * Returns a button string
   * depending on the form state.
   *
   * @return Number of files selected if any
   * or a placeholder otherwise
   */
  public showButtonText(): string {
    const videos = this.folderSettings.videos;

    return videos.length ? `${videos.length} Files Selected` : 'Video Files';
  }

  public handleCustomFiles(event: any) {
    this.folderSettings.videos = [...event.target.files];
  }

}

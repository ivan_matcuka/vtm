import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadMatchingFormComponent } from './upload-matching-form.component';

describe('UploadMatchingFormComponent', () => {
  let component: UploadMatchingFormComponent;
  let fixture: ComponentFixture<UploadMatchingFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadMatchingFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadMatchingFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

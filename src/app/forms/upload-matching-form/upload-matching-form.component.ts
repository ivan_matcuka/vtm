import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AudienceFolder, MatchingFolder, VideoFolder} from '../../folder';
import {FolderService} from '../../folder.service';
import {ArrayResourceResponse} from '../../interfaces/foldersResponse';

interface NewFormSettings {
  name: string;
  videoFolderID: number;
  audienceFolderID: number;
}

@Component({
  selector: 'app-upload-matching-form',
  templateUrl: './upload-matching-form.component.html',
  styleUrls: ['./upload-matching-form.component.styl']
})
export class UploadMatchingFormComponent implements OnInit {
  @Input() matchingFolders: VideoFolder[];
  @Output() created = new EventEmitter<MatchingFolder[]>();

  public audienceFolders: AudienceFolder[];

  public uploadFormSettings: NewFormSettings = {
    name: '',
    videoFolderID: null,
    audienceFolderID: null,
  };

  constructor(private folderService: FolderService) {
  }

  onSubmit() {
    // const newFolder: MatchingFolder = {
    //   name: this.newFormSettings.name,
    //   audienceFolderID: +this.newFormSettings.audienceFolderID,
    //   type: 'matching',
    //   videoFolderID: +this.newFormSettings.videoFolderID
    // };
    //
    // this.folderService.createMatching(newFolder)
    //   .subscribe((response: ArrayResourceResponse<MatchingFolder>) => {
    //     this.created.emit(response.data);
    //   });

  }

  ngOnInit() {
  }

}

export interface Video {
  creativeID: string;
  description: string;
  folderID: number;
  title: string;
  youtubeID: string;
}

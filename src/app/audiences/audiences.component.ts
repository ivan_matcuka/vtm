// Framework
import {Component, OnInit} from '@angular/core';
import {Title} from '@angular/platform-browser';

// Services
import {FolderService} from '../folder.service';

// Components
import {AudienceFolderFormComponent} from '../forms/audience-folder-form/audience-folder-form.component';

// Interfaces
import {ArrayResourceResponse} from '../interfaces/foldersResponse';
import {AudienceFolder} from '../folder';

// Libs
import moment from 'moment/src/moment';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-audiences',
  templateUrl: './audiences.component.html',
  styleUrls: ['./audiences.component.styl']
})
export class AudiencesComponent implements OnInit {
  /**
   * Audience folders.
   * Contain targets and have a campaign settings each.
   */
  public folders: AudienceFolder[];

  /**
   * AudiencesComponent constructor.
   *
   * @param folderService - Folder service DI
   * @param modalService - Modal service DI (Angular Bootstrap)
   * @param titleService - Title service DI
   */
  public constructor(
    private folderService: FolderService,
    private modalService: NgbModal,
    private titleService: Title,
  ) {
  }

  /**
   * Opens the form.
   * Adds a new folder and closes the form
   * after the "created" event is fired.
   */
  public openAddForm(): void {
    const modelRef = this.modalService.open(AudienceFolderFormComponent);

    modelRef.componentInstance.created.subscribe((audienceFolder: AudienceFolder) => {
      this.folders = [audienceFolder, ...this.folders];
      modelRef.close();
    });
  }

  /**
   * Deletes the folder and resets the
   * "folders" state.
   *
   * @param id - Folder numeric ID
   */
  public deleteFolder(id: number): void {
    this.folderService.delete(id)
      .subscribe(() => {
        this.folders = this.folders.filter((folder: AudienceFolder) => {
          return folder.id !== id;
        });
      });
  }

  /**
   * Returns a formatted date string.
   *
   * @param date - Raw date string
   */
  public formatDate(date: string): string {
    return moment(date).format('MMM Do YY');
  }

  /**
   * Returns a formatted "frequency" string.
   *
   * @param folder - Audience folder instance
   */
  public formatFrequency(folder: AudienceFolder): string {
    if (!folder.frequency) {
      return 'N/A';
    }

    return folder.frequency
      .split('_')
      .join(' ');
  }

  /** @inheritDoc */
  public ngOnInit() {
    this.folderService.all('audience')
      .subscribe((response: ArrayResourceResponse<AudienceFolder>) => {
        this.folders = response.data;
      });

    this.titleService.setTitle('Audience Folders');
  }
}

export interface TargetOptions {
  [key: string]: string;
}

export interface TargetField {
  key: string;
  name: string;
  options?: TargetOptions;
}

export interface SaveTargetsRequest extends Target {
  folderID: number;
  techAdGroupID: number;
}

export class Target {
  public id?: number;
  public location: string[] = [];
  public age: string[] = [];
  public gender: string[] = [];
  public interest: string[] = [];
  public topics: string[] = [];
  public languages: string[] = [];
  public keyword: string[] = [];
  public channelID: string[] = [];
  public adSchedule: string[] = [];
  public audience: string[] = [];
  public customAudience: string[] = [];
  public devices: string[] = [];
  public placement: string[] = [];
}

import {Component, OnInit} from '@angular/core';
import {FolderService} from '../folder.service';
import {ArrayResourceResponse, ResourceResponse} from '../interfaces/foldersResponse';
import {VideoFolder} from '../folder';
import {ActivatedRoute} from '@angular/router';
import {VideoFolderFormComponent} from '../forms/video-folder-form/video-folder-form.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {VideoFormComponent} from '../forms/video-form/video-form.component';
import {Video} from '../video';

@Component({
  selector: 'app-video-folder',
  templateUrl: './video-folder.component.html',
  styleUrls: ['./video-folder.component.styl']
})
export class VideoFolderComponent implements OnInit {
  public videos: Video[] = [];

  constructor(
    private folderService: FolderService,
    private route: ActivatedRoute,
    private modalService: NgbModal,
  ) {
  }

  public ngOnInit() {
    this.folderService.videoFolder(this.route.snapshot.params.id)
      .subscribe((response: Video[]) => {
        console.log(response);
        this.videos = response;
        console.log(this.videos);
      });
  }

  public openAddForm() {
    const modelRef = this.modalService.open(VideoFormComponent);

    modelRef.componentInstance.folderId = this.route.snapshot.params.id;

    modelRef.componentInstance.created.subscribe((videos: Video[]) => {
      this.videos = [...videos, ...this.videos];
      modelRef.close();
    });
  }

}


import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VideoFolderComponent } from './video-folder.component';

describe('VideoFolderComponent', () => {
  let component: VideoFolderComponent;
  let fixture: ComponentFixture<VideoFolderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VideoFolderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VideoFolderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

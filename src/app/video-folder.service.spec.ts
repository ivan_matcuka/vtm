import { TestBed } from '@angular/core/testing';

import { VideoFolderService } from './video-folder.service';

describe('VideoFolderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VideoFolderService = TestBed.get(VideoFolderService);
    expect(service).toBeTruthy();
  });
});
